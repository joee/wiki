To allow third party software to communicate with river, river provides
its own protocols. Protocols are XML files used to generate, with
the help of a _scanner_, the function prototypes that can be used by
clients and compositors. Protocols are language agnostic so you can
choose your favorite one to create your own application.

> /!\ Note that the language you chose need to have Wayland bindings.

## What do you want to do?

> /!\ Note that all of these protocols will likely be removed and replaced
> with something new before a stable river 1.0

Use [river-layout-v3](#river-layout-v3) if you want to:

-   Write the layout generator of your dream with the features you
    really want and use.

Use [river-control-unstable-v1](#river-control-unstable-v1) if you want to:

-   Send riverctl commands

Use [river-status-unstable-v1](#river-status-unstable-v1) if you want to:

-   Make a status bar, panel or everything with river tags information.

## Protocols

You can find all current river protocols in the `protocol/`
[folder](https://codeberg.org/river/river/src/branch/master/protocol)

Before we start, some terms to know:

-   interface: An object which defines what _requests_ and _events_ are possible.
-   request: Requests are sent from the client to the server.
-   event: Events are sent from the server to the client.

A **client** will listen for _events_ and sends _requests_, on the
contrary a **server** will listen for _requests_ and sends _events_.

If you want to write a third party software for river, river would
be the **server** and your application would be the **client**, river
will send you _events_ and your clients will respond with _request_.

More information about Wayland protocols in the [Wayland book](https://wayland-book.com/introduction.html)

### river-layout-v3

This protocol allow clients to specifies views dimensions and positions.

XML file: https://github.com/ifreund/river/blob/master/protocol/river-layout-v3.xml

---

**Interface**: `river_layout_manager_v3`

**Requests**:

-   `destroy`:

Let the server know that you will no longer use this interface
anymore. Previous requests sends are not impacted by this request.
This is a common _request_ that you'll see in all protocols.

-   `get_layout`:

Create the `river_layout_v3` object that will be used to allow
communications between the client and the server for layout related
messages. An unique _namespace_ per output is required.

---

**Interface**: `river_layout_v3`

**Requests**:

-   `destroy`:

Let the server know that you will no longer use this interface
anymore. Previous requests sends are not impacted by this request.

-   `push_views_dimensions`:

Once you client has received a `layout_demand` events and handle
information, decided how to place views, you send this request to
apply the proposed dimensions and positions of a view identified by
the same `serial` than in the `layout_demand`. This request should
be sent for every view part of the `layout_demand`

-   `commit`:

Tell river that your client has finished pushing dimensions
(`push_views_dimensions`) and that it can now apply the layout for
all views in the `layout_demand`.

**Events**:

-   `namespace_in_use`:

Directly send after the creation of the `river_layout_v3` object,
it ensures that only the requests sent by your object will be taken
into account.

-   `layout_demand`:

River will send this event to let you know that some view
requires a layout, dimensions and positions. This is the moment
where you decide how you want your layout client to place views on
your screen.

River will send you some information to use:

-   view_count: Number of views in the layout.
-   usable_width: This is the width you can safely use for placing your views.
-   usable_height: This is the height you can safely use for placing your views.
-   tags: Focused tags of the output.
-   serial: It is use to identify request, multiple `layout_demand`
    at the same time are possible.

The server will only take into account the most recent response to a
`layout_demand`.

-   `user_command`:

It informs you that a user of your layout client send a command. There is
no command implemented in the protocol itself. it's up to you to do
all the work if you want to use this. You can also ignore it.

---

To resume, the step to make your client are:

1. You create an object with `get_layout` so the client and the server
   understand each other.
2. River check that your namespace is unique, if not tell it what to
   do in `namespace_in_use`.
3. River will sends you a `layout_demand` for a set of views that need
   a layout. This is the important part, the one that probably interest
   you. The moment you chose how your layout client differs from others
   layout generator.
4. You `push_views_dimensions` for the same set of views that river
   want you to handle in `layout_demand`.

@Leon-Plickat wrote a great article about river layout protocol
[here](https://leon_plickat.srht.site/blog/explaining-river-window-layouts/article.html)
, I can't recommend enough to read it if you want a more in-depth
knowledge.

### river-control-unstable-v1

This protocol allow clients to send river specific commands and receive
a success/failure response. Basically using `riverctl` from a client.

XML file: https://github.com/ifreund/river/blob/master/protocol/river-control-unstable-v1.xml

### river-status-unstable-v1

This protocol is used to get information specific to river. Tags state
(visible, focused, occupied), focused view title and so on.

XML file: https://github.com/ifreund/river/blob/master/protocol/river-status-unstable-v1.xml
